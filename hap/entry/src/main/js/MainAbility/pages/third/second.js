/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
import router from '@system.router'

import util from '@ohos.util'
export default {
    data: {
        info: 'World'
    },
    log(s) {
        this.info += "\n" + s;
    },
    onclick: function () {
        let code = [83
, 116
, 97
, 114
, 116
, 46
, 229
, 155
, 189
, 229
, 174
, 182
, 231
, 130
, 185]

        let ui8 = new Uint8Array(code);
        let utf8decoder = new util.TextDecoder("utf-8", {
            fatal: true
        });
        let str = utf8decoder.decode(ui8);
        console.log('openblock getString '+str);
        let cache = {};
        cache[4] = str;
        setTimeout(()=>{

            let ostr = cache[4];
            console.log('openblock String '+ostr);
            console.log('openblock comp String '+(str==ostr));
            let obj = {};
            obj[str] = "abc";

            setTimeout(()=>{
                let value = obj[ostr];
                console.log('openblock value:'+value);
                obj[ostr] = "bcd";
                value = obj[str];
                console.log('openblock value:'+value);
                value = obj[ostr];
                console.log('openblock value:'+value);

                let value1 = JSON.parse(JSON.stringify(value));
                console.log('openblock value1:'+value1);
                console.log('openblock comp value01:'+(value==value1));

                value = 'Start.国家点';
                value1 = cache[4];
                console.log('openblock value1:'+value1);
                console.log('openblock comp value01:'+(value==value1));
            },5);
        },5);
    }
}
